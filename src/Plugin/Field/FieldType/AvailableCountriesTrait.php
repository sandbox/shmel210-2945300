<?php

namespace Drupal\address\Plugin\Field\FieldType;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AvailableCountriesEvent;
use Drupal\Core\Form\FormStateInterface;

/**
 * Allows field types to limit the available countries.
 */
trait AvailableCountriesTrait {

  /**
   * An altered list of available countries.
   *
   * @var array
   */
  protected static $availableCountries = [];

  /**
   * Defines the default field-level settings.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public static function defaultCountrySettings() {
    return [
      'available_countries' => [],
    ];
  }

  /**
   * Builds the field settings form.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The modified form.
   */
  public function countrySettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['available_countries'] = [
      '#type' => 'select',
      '#title' => $this->t('Available countries'),
      '#description' => $this->t('If no countries are selected, all countries will be available.'),
      '#options' => \Drupal::service('address.country_repository')->getList(),
      '#default_value' => $this->getSetting('available_countries'),
      '#multiple' => TRUE,
      '#size' => 10,
    ];

    return $element;
  }

  /**
   * Gets the available countries for the current field.
   *
   * @return array
   *   A list of country codes.
   */
  public function getAvailableCountries() {
    // Alter the list once per field, instead of once per field delta.
    $field_definition = $this->getFieldDefinition();
    $definition_id = spl_object_hash($field_definition);
    if (!isset(static::$availableCountries[$definition_id])) {
      $available_countries = array_filter($this->getSetting('available_countries'));
      $event_dispatcher = \Drupal::service('event_dispatcher');
      $event = new AvailableCountriesEvent($available_countries, $field_definition);
      $event_dispatcher->dispatch(AddressEvents::AVAILABLE_COUNTRIES, $event);
      static::$availableCountries[$definition_id] = $event->getAvailableCountries();
    }

    return static::$availableCountries[$definition_id];
  }

  public static function getDeliveryMethods() {
    return [
      'new_post_at_department' => t('New post at department'),
      'new_post_at_address' => t('New post at address'),
      'pickup' => t('Pickup'),
//      'privatbank_postmat' => t('Privatbank postmat'),
    ];
  }
  public static function getDefaultDeliveryMethod() {
    return 'new_post_at_department';
  }
  public static function getNewpostDepartments($locality) {
    if(!$locality) {
      return [];
    }
    $api_key = '5239e7665aa5aea917a29f2230c115f7';
    $departments = [];
    $curl = curl_init();
    $data = [
      'modelName' => 'AddressGeneral',
      'calledMethod' => 'getWarehouses',
      'methodProperties' => ['Language' => 'ru', 'CityName' => $locality],
      'apiKey' =>  $api_key,
    ];
    $warehouse_types = [
      '6f8c7162-4b72-4b0a-88e5-906948c6a92f' => 'Parcel Shop',
      '841339c7-591a-42e2-8233-7a0a00f0ed6f' => 'Почтовое отделение',
      '9a68df70-0267-42a8-bb5c-37f427e36ee4' => 'Грузовое отделение',
      'f9316480-5f2d-425d-bc2c-ac7cd29decf0' => 'Почтомат',
    ];
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/",
      CURLOPT_RETURNTRANSFER => True,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array("content-type: application/json",),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      echo "";
      return [];
    } else {
      $response = json_decode($response, TRUE);

      foreach($response['data'] as $department) {
        if(isset($warehouse_types[$department['TypeOfWarehouse']])) {
          $departments[$department['Description']] = $department['Description'];
        }
      }
    }
    return $departments;
  }
  public static function getPrivatbankDepartments($locality) {
    if(!$locality) {
      return [];
    }
    $api_key = '338e466eb68ced9b03c6752f00cc32ab';
    $departments = [];
    $curl = curl_init();
    $data = [
      'modelName' => 'AddressGeneral',
      'calledMethod' => 'getWarehouses',
      'methodProperties' => ['Language' => 'ru', 'CityName' => $locality],
      'apiKey' =>  $api_key,
    ];
    $warehouse_types = [
      '95dc212d-479c-4ffb-a8ab-8c1b9073d0bc' => 'Почтомат приват банка',
    ];
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/",
      CURLOPT_RETURNTRANSFER => True,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array("content-type: application/json",),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      echo "";
      return [];
    } else {
      $response = json_decode($response, TRUE);

      foreach($response['data'] as $department) {
        if(isset($warehouse_types[$department['TypeOfWarehouse']])) {
          $departments[$department['Description']] = $department['Description'];
        }
      }
    }
    return $departments;
  }

}
